/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.talha.hellostruts.action;

import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.ResultPath;

/**
 *
 * @author mahbuburrubtalha
 */


@Namespace("/User")
@ResultPath("/")
@Result(name = "success", location = "home.jsp")
public class HomeAction extends ActionSupport{
    
}
